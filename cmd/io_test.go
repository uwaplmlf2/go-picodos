package cmd

import (
	"bytes"
	"context"
	"os"
	"testing"
	"time"
)

var LISTING = `dir
Directory of A:
TESTMODE.SCM         15,488 01-09-18 19:35
IOTEST~1.RUN         72,600 04-26-05 08:34
IOTEST.RUN           71,552 10-14-14 11:33

     252,190,720 bytes free
PicoDOS>`

func timeParse(s string) time.Time {
	t, _ := time.Parse(time.RFC3339, s)
	return t
}

func TestDir(t *testing.T) {
	entries := []Entry{
		{
			filename: "TESTMODE.SCM",
			size:     15488,
			mtime:    timeParse("2018-01-09T19:35:00Z"),
		},
		{
			filename: "IOTEST~1.RUN",
			size:     72600,
			mtime:    timeParse("2005-04-26T08:34:00Z"),
		},
		{
			filename: "IOTEST.RUN",
			size:     71552,
			mtime:    timeParse("2014-10-14T11:33:00Z"),
		},
	}

	src := bytes.NewBufferString(LISTING)
	tokens, err := ReadUntilSeq(context.TODO(), src, "PicoDOS", ">")
	if err != nil {
		t.Fatal(err)
	}

	if len(tokens) != 50 {
		t.Errorf("Wrong token count; expected 50, got %d", len(tokens))
	}

	d, err := ParseDirectory(tokens)
	if err != nil {
		t.Fatal(err)
	}

	for i, e := range entries {
		if e != *(d.Files[i]) {
			t.Errorf("Bad entry (%d); expected %v, got %v", i, e, d.Files[i])
		}
	}

	var fe interface{} = d.Files[0]
	_, ok := fe.(os.FileInfo)
	if !ok {
		t.Errorf("*Entry does not implement the os.FileInfo interface")
	}
}

func ExampleDir() {
	src := bytes.NewBufferString(LISTING)
	tokens, err := ReadUntilSeq(context.TODO(), src, "PicoDOS", ">")
	if err == nil {
		d, err := ParseDirectory(tokens)
		if err == nil {
			showFiles(d.GetFileInfo(), nil, os.Stdout)
		}
	}

	// Output:
	//    15488 2018-01-09 19:35 testmode.scm
	//    72600 2005-04-26 08:34 iotest~1.run
	//    71552 2014-10-14 11:33 iotest.run
}
