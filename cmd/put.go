package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// putCmd represents the put command
var putCmd = &cobra.Command{
	Use:          "put file [file...]",
	Short:        "Upload one or more files to a float",
	Args:         cobra.MinimumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		sh, err := openShell()
		if err != nil {
			return err
		}

		err = sh.SetBaud(viper.GetInt("xferbaud"))
		if err != nil {
			return fmt.Errorf("Cannot change baud rate: %v", err)
		}
		defer sh.SetBaud(viper.GetInt("baud"))

		for _, arg := range args {
			err = sh.Ymput(context.Background(), arg)
			if err != nil {
				return fmt.Errorf("Upload failed: %v", err)
			}
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(putCmd)
}
