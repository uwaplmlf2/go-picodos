package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// delCmd represents the del command
var delCmd = &cobra.Command{
	Use:          "del file [file ...]",
	Aliases:      []string{"rm"},
	Short:        "Delete one or more files",
	Args:         cobra.MinimumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		sh, err := openShell()
		if err != nil {
			return err
		}

		for _, arg := range args {
			resp, err := sh.Delete(arg)
			if err != nil {
				return fmt.Errorf("del %q error: %v", arg, err)
			}
			if resp == "" {
				fmt.Printf("%s: removed\n", arg)
			} else {
				fmt.Printf("%s: %s\n", arg, resp)
			}
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(delCmd)
}
