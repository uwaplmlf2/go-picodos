package cmd

import (
	"fmt"
	"os"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile   string
	scanDebug bool
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "picodos",
	Short: "Transfer files to/from an MLF2 float",
	Long: `Picodos provides a CLI to list, upload to, and download from
an MLF2 file system.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	rootCmd.Version = version
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		"config file (default is $HOME/.picodos.yaml)")
	rootCmd.PersistentFlags().BoolVar(&scanDebug, "debug", false, "enable debugging output")
	rootCmd.PersistentFlags().StringP("device", "d", "/dev/ttyS0", "Serial device to use")
	rootCmd.PersistentFlags().IntP("baud", "b", 9600, "serial baud rate")
	rootCmd.PersistentFlags().Int("xferbaud", 115200, "serial baud rate for file transfers")
	rootCmd.PersistentFlags().Bool("nocheck", false, "if true, do not check for PicoDOS prompt")
	viper.BindPFlag("device", rootCmd.PersistentFlags().Lookup("device"))
	viper.BindPFlag("baud", rootCmd.PersistentFlags().Lookup("baud"))
	viper.BindPFlag("xferbaud", rootCmd.PersistentFlags().Lookup("xferbaud"))
	viper.BindPFlag("nocheck", rootCmd.PersistentFlags().Lookup("nocheck"))
	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".picodos" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".picodos")
	}

	viper.SetEnvPrefix("picodos")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func openShell() (*pdShell, error) {
	var (
		err error
		p   Port
	)

	device := viper.GetString("device")
	if strings.Contains(device, ":") {
		p, err = NetworkPort(device)
	} else {
		p, err = SerialPort(device, viper.GetInt("baud"))
	}

	if err != nil {
		return nil, fmt.Errorf("Could not connect: %v", err)
	}

	sh, err := newShell(p)
	if err != nil {
		return nil, err
	}

	if !viper.GetBool("nocheck") {
		err = sh.Check()
		if err != nil {
			return nil, fmt.Errorf("PicoDOS not running: %v", err)
		}
	}

	return sh, nil
}
