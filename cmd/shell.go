package cmd

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"text/scanner"
	"time"
	"unicode"

	"github.com/pkg/errors"
)

const (
	DirStart    int           = 5
	promptDelay time.Duration = time.Millisecond * 250
)

type Entry struct {
	filename string
	size     int
	mtime    time.Time
}

// Name implements the os.FileInfo interface
func (e *Entry) Name() string { return e.filename }

// Size implements the os.FileInfo interface
func (e *Entry) Size() int64 { return int64(e.size) }

// ModTime implements the os.FileInfo interface
func (e *Entry) ModTime() time.Time { return e.mtime }

// Mode implements the os.FileInfo interface
func (e *Entry) Mode() os.FileMode { return os.ModePerm }

// IsDir implements the os.FileInfo interface
func (e *Entry) IsDir() bool { return false }

// Sys implements the os.FileInfo interface
func (e *Entry) Sys() interface{} { return nil }

type Directory struct {
	Files []*Entry
	Free  int
}

func (d *Directory) GetFileInfo() []os.FileInfo {
	fi := make([]os.FileInfo, len(d.Files))
	for i, f := range d.Files {
		fi[i] = f
	}

	return fi
}

type Token struct {
	Type  rune
	Value string
}

func parseTime(tokens []Token) (time.Time, int, error) {
	var t time.Time
	if len(tokens) < 8 {
		return t, 0, fmt.Errorf("Not enough tokens")
	}

	vals := make([]int, 0, 5)
	for i := 0; i < 8; i++ {
		if tokens[i].Type == scanner.Int {
			x, _ := strconv.ParseInt(tokens[i].Value, 10, 32)
			vals = append(vals, int(x))
		}
	}

	if len(vals) != 5 {
		return t, 0, fmt.Errorf("Invalid time format")
	}

	t = time.Date(vals[2]+2000, time.Month(vals[0]), vals[1],
		vals[3], vals[4], 0, 0, time.UTC)

	return t, 8, nil
}

func parseEntry(tokens []Token) (*Entry, int, error) {
	i := int(0)

	if len(tokens) < 2 {
		return nil, i, fmt.Errorf("Not enough tokens")
	}

	if tokens[i].Type != scanner.Ident {
		return nil, i, fmt.Errorf("No filename")
	}

	e := Entry{filename: strings.Trim(tokens[i].Value, " ")}
	i++
	if tokens[i].Type != scanner.Int {
		return nil, i, fmt.Errorf("No file size")
	}
	e.size, _ = strconv.Atoi(tokens[i].Value)

	i++
	for i < len(tokens) && tokens[i].Type == ',' {
		e.size *= 1000
		i++
		x, err := strconv.Atoi(tokens[i].Value)
		if err != nil {
			return &e, i, errors.Wrap(err, "parse file size")
		}
		e.size += x
		i++
	}

	var (
		inc int
		err error
	)

	e.mtime, inc, err = parseTime(tokens[i:])

	return &e, i + inc, err
}

func ParseDirectory(tokens []Token) (*Directory, error) {
	var (
		i int
	)
	d := Directory{Files: make([]*Entry, 0)}
	for i = 0; i < len(tokens); i++ {
		if tokens[i].Value == ":" {
			i++
			break
		}
	}

	if i >= len(tokens) {
		return nil, fmt.Errorf("No directory listing found")
	}

	for i < len(tokens) {
		e, inc, err := parseEntry(tokens[i:])
		if inc == 0 || err != nil {
			break
		}
		d.Files = append(d.Files, e)
		i += inc
	}

	if i >= len(tokens) || tokens[i].Type != scanner.Int {
		return &d, fmt.Errorf("No free-space size")
	}
	d.Free, _ = strconv.Atoi(tokens[i].Value)

	for i < len(tokens) && tokens[i].Type == ',' {
		d.Free *= 1000
		i++
		x, err := strconv.Atoi(tokens[i].Value)
		if err != nil {
			return &d, errors.Wrap(err, "parse free space")
		}
		d.Free += x
		i++
	}

	return &d, nil
}

func ReadUntilSeq(ctx context.Context, rdr io.Reader, seq ...string) ([]Token, error) {
	var s scanner.Scanner
	s.Init(rdr)
	s.IsIdentRune = func(ch rune, i int) bool {
		return ch == '_' || ch == '~' || unicode.IsLetter(ch) ||
			(unicode.IsDigit(ch) && i > 0) || (ch == '.' && i > 0)
	}
	s.Error = func(_ *scanner.Scanner, _ string) {}

	tokens := make([]Token, 0)
	i := int(0)
	for {
		tok := s.Scan()
		if tok == scanner.EOF {
			return tokens, nil
		}
		text := s.TokenText()
		if scanDebug {
			log.Printf("Token{%v, %q}", tok, text)
		}
		tokens = append(tokens, Token{Type: tok, Value: text})
		if text == seq[i] {
			i++
			if i == len(seq) {
				return tokens, nil
			}
		} else {
			i = 0
		}

		select {
		case <-ctx.Done():
			return tokens, ctx.Err()
		default:
		}
	}

	return tokens, fmt.Errorf("Sequence %q not found", seq)
}

type pdShell struct {
	port             Port
	rb_path, sb_path string
	mark             time.Time
}

func newShell(p Port) (*pdShell, error) {
	var err error
	s := &pdShell{port: p}
	s.rb_path, err = exec.LookPath("rb")
	if err != nil {
		s.rb_path, err = exec.LookPath("lrb")
		if err != nil {
			return nil, errors.New("YMODEM receive command not found")
		}
	}
	s.sb_path, err = exec.LookPath("sb")
	if err != nil {
		s.sb_path, err = exec.LookPath("lsb")
		if err != nil {
			return nil, errors.New("YMODEM send command not found")
		}
	}
	s.setMark(promptDelay)

	return s, nil
}

func (s *pdShell) setMark(d time.Duration) {
	s.mark = time.Now().Add(d)
}

func (s *pdShell) waitForMark() {
	time.Sleep(time.Until(s.mark))
}

// Check verifies the serial connection
func (s *pdShell) Check() error {
	s.waitForMark()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	s.port.Write([]byte("\r"))
	_, err := ReadUntilSeq(ctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	return err
}

// SetBaud changes the baud rate of the serial connection
func (s *pdShell) SetBaud(baud int) error {
	if !s.port.CanChangeBaud() {
		return nil
	}
	s.waitForMark()
	cmd := fmt.Sprintf("baud %d\r", baud)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	s.port.EchoMode(true)
	s.port.Write([]byte(cmd))
	s.port.EchoMode(false)

	_, err := ReadUntilSeq(ctx, s.port, "when")
	time.Sleep(time.Second)
	if err != nil {
		return err
	}
	s.port.Write([]byte("\r"))

	err = s.port.SetBaud(baud)
	if err != nil {
		return err
	}

	_, err = ReadUntilSeq(ctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	return err
}

// Dir returns the directory listing.
func (s *pdShell) Dir() ([]os.FileInfo, error) {
	s.waitForMark()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	s.port.EchoMode(true)
	s.port.Write([]byte("dir\r"))
	s.port.EchoMode(false)

	tokens, err := ReadUntilSeq(ctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	d, err := ParseDirectory(tokens)
	if err != nil {
		return nil, err
	}

	return d.GetFileInfo(), nil
}

// Ymget downloads a file using YMODEM
func (s *pdShell) Ymget(pctx context.Context, filename string) error {
	s.waitForMark()
	text := fmt.Sprintf("ys %s\r", filename)
	ctx, cancel := context.WithTimeout(pctx, time.Second*5)
	defer cancel()

	s.port.EchoMode(true)
	s.port.Write([]byte(text))
	s.port.EchoMode(false)

	dosFilename := strings.ToUpper(filename)
	_, err := ReadUntilSeq(ctx, s.port, "Sending", ":")
	if err != nil {
		return err
	}

	log.Printf("Downloading %s ... ", filename)
	cmd := exec.CommandContext(pctx, s.rb_path)
	cmd.Stdin = s.port
	cmd.Stdout = s.port
	cmd.Stderr = s.port
	err = cmd.Start()
	if err != nil {
		return err
	}
	err = cmd.Wait()
	if err != nil {
		log.Printf("Download error: %v", err)
	} else {
		// Rename file from uppercase
		os.Rename(dosFilename, filename)
	}

	_, err = ReadUntilSeq(pctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	return err
}

// Ymput uploads a file using YMODEM
func (s *pdShell) Ymput(pctx context.Context, pathname string) error {
	filename := filepath.Base(pathname)

	s.waitForMark()
	s.port.Write([]byte("yr -Q\r"))
	time.Sleep(time.Second * 2)

	log.Printf("Uploading %s -> %q ... ", pathname, filename)
	cmd := exec.CommandContext(pctx, s.sb_path, filename)
	cmd.Dir = filepath.Dir(pathname)
	cmd.Stdin = s.port
	cmd.Stdout = s.port
	cmd.Stderr = os.Stderr
	err := cmd.Start()
	if err != nil {
		return err
	}
	err = cmd.Wait()
	if err != nil {
		log.Printf("Upload error: %v", err)
	}

	_, err = ReadUntilSeq(pctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	return err
}

// Delete removes a file from the filesystem
func (s *pdShell) Delete(filename string) (string, error) {
	s.waitForMark()

	cmd := fmt.Sprintf("del %s\r", filename)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	s.port.EchoMode(true)
	s.port.Write([]byte(cmd))
	s.port.EchoMode(false)

	tokens, err := ReadUntilSeq(ctx, s.port, "PicoDOS")
	s.setMark(promptDelay)
	if err != nil {
		return "", err
	}

	if len(tokens) > 3 {
		resp := make([]string, 0, len(tokens)-1)
		for i, token := range tokens[:len(tokens)-1] {
			if i >= 2 {
				resp = append(resp, token.Value)
			}
		}
		return strings.Join(resp, " "), nil
	}

	return "", nil
}
