package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get file [file ...]",
	Short:        "Download one or more files from PicoDOS",
	Args:         cobra.MinimumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		sh, err := openShell()
		if err != nil {
			return err
		}

		err = sh.SetBaud(viper.GetInt("xferbaud"))
		if err != nil {
			return fmt.Errorf("Cannot change baud rate: %v", err)
		}
		defer sh.SetBaud(viper.GetInt("baud"))

		for _, arg := range args {
			err = sh.Ymget(context.Background(), arg)
			if err != nil {
				return fmt.Errorf("Download failed: %v", err)
			}
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(getCmd)
}
