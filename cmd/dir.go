package cmd

import (
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/gobwas/glob"
	"github.com/spf13/cobra"
)

var dirCmd = &cobra.Command{
	Use:          "dir [pattern]",
	Aliases:      []string{"ls"},
	Short:        "list the contents of an MLF2 file system",
	Args:         cobra.MaximumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		sh, err := openShell()
		if err != nil {
			return err
		}

		files, err := sh.Dir()
		if len(args) > 0 {
			g, err := glob.Compile(args[0])
			if err != nil {
				return fmt.Errorf("Invalid pattern: %v", err)
			}
			showFiles(files, g, os.Stdout)
		} else {
			showFiles(files, nil, os.Stdout)
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(dirCmd)
}

const timeFormat = "2006-01-02 15:04"

func showFiles(files []os.FileInfo, g glob.Glob, w io.Writer) {
	if g == nil {
		for _, f := range files {
			fmt.Fprintf(w, "%8d %s %-12s\n",
				f.Size(),
				f.ModTime().Format(timeFormat),
				strings.ToLower(f.Name()))
		}
	} else {
		for _, f := range files {
			if name := strings.ToLower(f.Name()); g.Match(name) {
				fmt.Fprintf(w, "%8d %s %-12s\n",
					f.Size(),
					f.ModTime().Format(timeFormat),
					name)
			}
		}
	}
}
