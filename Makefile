DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

APP := picodos

all: darwin linux

dep:
	@go get -v -d ./...

build = GOOS=$(1) GOARCH=$(2) go build \
	-o $(APP)-$(3)-$(1)-$(2)$(4) \
	-tags release \
	-ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'

linux: $(APP)-$(VERSION)-linux-amd64

darwin: $(APP)-$(VERSION)-darwin-amd64

$(APP)-$(VERSION)-linux-amd64: dep
	CGO_ENABLED=0 $(call build,linux,amd64,$(VERSION))

$(APP)-$(VERSION)-darwin-amd64: dep
	$(call build,darwin,amd64,$(VERSION))

clean:
	rm -f $(APP)-*
