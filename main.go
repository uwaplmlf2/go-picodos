package main

import "bitbucket.org/uwaplmlf2/go-picodos/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
