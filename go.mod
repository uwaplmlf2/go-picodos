module bitbucket.org/uwaplmlf2/go-picodos

go 1.13

require (
	github.com/gobwas/glob v0.2.3
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20220712014510-0a85c31ab51e // indirect
)
